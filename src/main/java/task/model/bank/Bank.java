package task.model.bank;

import task.model.bank.helpers.*;

public class Bank {
    private Integer bankNum;
    private MyName name;
    private MyCountry country;
    private MyType type;
    private MyDepositor myDepositor;
    private MyAccountID accountID;
    private MyProfitability profitability;
    private MyAmountOnDeposit amountOnDeposit;
    private MyConstraints constraints;

    public Bank(){
    }

    public Bank(Integer bankNum, MyName name, MyCountry country, MyType type, MyDepositor myDepositor, MyAccountID accountID, MyProfitability profitability, MyAmountOnDeposit amountOnDeposit, MyConstraints constraints) {
        this.bankNum = bankNum;
        this.name = name;
        this.country = country;
        this.type = type;
        this.myDepositor = myDepositor;
        this.accountID = accountID;
        this.profitability = profitability;
        this.amountOnDeposit = amountOnDeposit;
        this.constraints = constraints;
    }

    public Integer getBankNum() {
        return bankNum;
    }

    public void setBankNum(Integer bankNum) {
        this.bankNum = bankNum;
    }

    public MyName getName() {
        return name;
    }

    public void setName(MyName name) {
        this.name = name;
    }

    public MyCountry getCountry() {
        return country;
    }

    public void setCountry(MyCountry country) {
        this.country = country;
    }

    public MyType getType() {
        return type;
    }

    public void setType(MyType type) {
        this.type = type;
    }

    public MyDepositor getMyDepositor() {
        return myDepositor;
    }

    public void setMyDepositor(MyDepositor myDepositor) {
        this.myDepositor = myDepositor;
    }

    public MyAccountID getAccountID() {
        return accountID;
    }

    public void setAccountID(MyAccountID accountID) {
        this.accountID = accountID;
    }

    public MyProfitability getProfitability() {
        return profitability;
    }

    public void setProfitability(MyProfitability profitability) {
        this.profitability = profitability;
    }

    public MyAmountOnDeposit getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(MyAmountOnDeposit amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public MyConstraints getConstraints() {
        return constraints;
    }

    public void setConstraints(MyConstraints constraints) {
        this.constraints = constraints;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "bankNum=" + bankNum +
                ", name=" + name +
                ", country=" + country +
                ", type=" + type +
                ", myDepositor=" + myDepositor +
                ", accountID=" + accountID +
                ", profitability=" + profitability +
                ", amountOnDeposit=" + amountOnDeposit +
                ", constraints=" + constraints +
                '}';
    }
}
