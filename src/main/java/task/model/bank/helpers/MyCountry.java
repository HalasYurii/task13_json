package task.model.bank.helpers;

public class MyCountry {
    private String name;

    public MyCountry(){
    }
    public MyCountry(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MyCountry{" +
                "name='" + name + '\'' +
                '}';
    }
}
