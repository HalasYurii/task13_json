package task.model.bank.helpers;

public class MyAccountID {
    private String id;

    public MyAccountID(){
    }

    public MyAccountID(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MyAccountID{" +
                "id='" + id + '\'' +
                '}';
    }
}
