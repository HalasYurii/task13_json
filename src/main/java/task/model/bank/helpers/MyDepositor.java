package task.model.bank.helpers;

public class MyDepositor {
    private String name;

    public MyDepositor() {
    }

    public MyDepositor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MyDepositor{" +
                "name='" + name + '\'' +
                '}';
    }
}
