package task.model.bank.helpers;

public class MyConstraints {
    private Double amount;
    private String type;

    public MyConstraints(){
    }

    public MyConstraints(Double amount, String type) {
        this.amount = amount;
        this.type = type;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "MyConstraints{" +
                "amount=" + amount +
                ", type='" + type + '\'' +
                '}';
    }
}
