package task.model.bank.helpers;

public class MyAmountOnDeposit {
    private Integer count;

    public MyAmountOnDeposit(){
    }

    public MyAmountOnDeposit(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "MyAmountOnDeposit{" +
                "count=" + count +
                '}';
    }
}
