package task.model.bank.helpers;

public class MyProfitability {
    private Double profitability;

    public MyProfitability() {
    }

    public MyProfitability(Double profitability) {
        this.profitability = profitability;
    }

    public Double getProfitability() {
        return profitability;
    }

    public void setProfitability(Double profitability) {
        this.profitability = profitability;
    }

    @Override
    public String toString() {
        return "MyProfitability{" +
                "profitability=" + profitability +
                '}';
    }
}
