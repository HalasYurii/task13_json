package task.model.bank.helpers;

public class MyType {
    private String name;

    public MyType(){
    }

    public MyType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MyType{" +
                "name='" + name + '\'' +
                '}';
    }
}
