package task.model.consts;

import com.google.gson.reflect.TypeToken;
import task.model.bank.Bank;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

import static task.model.consts.ConstWays.WAY_BANK_JSON;

public class ConstObject {
    public static final File FILE_JSON_BANK = new File(WAY_BANK_JSON);
    public static final String EMPTY_STRING = "";
    public static final String FUNCTION_START = "*****";
    public static final String FUNCTION_END = "*****";
    public static final Type TYPE = new TypeToken<List<Bank>>() {
    }.getType();



    private ConstObject() {
    }
}
