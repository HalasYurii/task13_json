package task.model.consts;

public class ConstWays {
    public static final String WAY_BANK_JSON
            = "C:\\Java Projects\\task13_JSON\\src\\main\\resources\\json\\jsonBanks.json";

    public static final String WAY_BANK_JSON_SCHEMA
            = "C:\\Java Projects\\task13_JSON\\src\\main\\resources\\json\\jsonBanksSchema.json";

    private ConstWays() {
    }
}
