package task.controller.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

import static task.controller.helpers.ShowString.showString;
import static task.model.consts.ConstDescriptionException.IO_EXCEPTION;
import static task.model.consts.ConstObject.FUNCTION_END;
import static task.model.consts.ConstObject.FUNCTION_START;

public class ReaderFile {
    private static final Logger LOG = LogManager.getLogger(ReaderFile.class);

    private static void showHelper(boolean isShow, StringBuilder text) {
        if (isShow) {
            showString(text.toString());
        }
    }

    public static String readByBufferedReader(File file, boolean isShowFile) {
        LOG.debug(FUNCTION_START + "ReadByBufferedReader" + FUNCTION_END);
        StringBuilder textFile = new StringBuilder();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(file)))) {
            while (br.ready()) {
                textFile.append(br.readLine());
            }
        } catch (IOException e) {
            LOG.error(IO_EXCEPTION);
            e.printStackTrace();
        }
        showHelper(isShowFile, textFile);
        return textFile.toString();
    }
}
