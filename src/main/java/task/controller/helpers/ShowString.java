package task.controller.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShowString {
    private static final Logger LOG = LogManager.getLogger(ShowString.class);

    public static void showString(String textOutput) {
        LOG.info(textOutput);
    }
}
