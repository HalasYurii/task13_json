package task.controller.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task.model.bank.Bank;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static task.model.consts.ConstObject.*;

public class JacksonParser {
    private static final Logger LOG = LogManager.getLogger(JacksonParser.class);
    private ObjectMapper mapper;

    JacksonParser(){
        mapper = new ObjectMapper();
    }

    String toJsonFromOjectToString(List<Bank> banks){
        LOG.debug(FUNCTION_START+"readJsonFromStringToObject"+FUNCTION_END);

        String jsonText = EMPTY_STRING;
        try {
            jsonText = mapper.writeValueAsString(banks);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonText;
    }

    List<Bank> fromJsonToObject(String jsonText){
        LOG.debug(FUNCTION_START+"fromJsonToObject"+FUNCTION_END);
        List<Bank> banks = new ArrayList<>();
        try {
            banks = mapper.readValue(jsonText, List.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return banks;
    }
}
