package task.controller.jackson;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task.model.bank.Bank;

import java.util.List;

import static task.controller.helpers.ReaderFile.readByBufferedReader;
import static task.model.consts.ConstObject.FILE_JSON_BANK;

public class RunJackson {
    private static final Logger LOG = LogManager.getLogger(RunJackson.class);

    public static void run(){
        String json = readByBufferedReader(FILE_JSON_BANK,false);
        JacksonParser jacksonParser = new JacksonParser();
        List<Bank> bankList = jacksonParser.fromJsonToObject(json);
        String jsonAfterReadJson = jacksonParser.toJsonFromOjectToString(bankList);
        LOG.info(jsonAfterReadJson);
    }
}
