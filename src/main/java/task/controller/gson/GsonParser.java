package task.controller.gson;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task.model.bank.Bank;

import java.util.List;

import static task.model.consts.ConstObject.FUNCTION_END;
import static task.model.consts.ConstObject.FUNCTION_START;
import static task.model.consts.ConstObject.TYPE;

class GsonParser {
    private final Logger LOG = LogManager.getLogger(GsonParser.class);
    private Gson gson;

    public GsonParser() {
        gson = new Gson();
    }

    List<Bank> fromJsonReadBanks(String json) {
        LOG.debug(FUNCTION_START + "fromJsonReadBanksByGsonParser*****" + FUNCTION_END);
        return gson.fromJson(json, TYPE);
    }

    String toJsonGetString(Object obj) {
        LOG.debug(FUNCTION_START + "toJsonGetString" + FUNCTION_END);
        return gson.toJson(obj);
    }
}

