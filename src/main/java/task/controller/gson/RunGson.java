package task.controller.gson;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task.model.bank.Bank;

import java.util.List;

import static task.controller.helpers.ReaderFile.readByBufferedReader;
import static task.model.consts.ConstObject.FILE_JSON_BANK;

public class RunGson {
    private static final Logger LOG = LogManager.getLogger(RunGson.class);

    public static void run() {
        String json = readByBufferedReader(FILE_JSON_BANK, false);
        GsonParser gsonParser = new GsonParser();
        List<Bank> banks = gsonParser.fromJsonReadBanks(json);
        String jsonAfterSetObject = gsonParser.toJsonGetString(banks);
        LOG.info(jsonAfterSetObject);
    }
}