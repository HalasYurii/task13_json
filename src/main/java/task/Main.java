package task;


import task.controller.gson.RunGson;
import task.controller.jackson.RunJackson;

import static task.controller.validators.MyValidator.runMyValidator;

public class Main {
    public static void main(String[] args) {
        runMyValidator();
        RunGson.run();
        RunJackson.run();
    }
}
